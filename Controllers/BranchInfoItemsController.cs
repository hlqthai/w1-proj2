using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using SalesManagementAPI.Models;

namespace SalesManagementAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class BranchInfoItemsController : ControllerBase
{
    private readonly BranchInfoContext _context;

    public BranchInfoItemsController(BranchInfoContext context) => _context = context;

    // GET: api/BranchInfoItems
    [HttpGet]
    public async Task<ActionResult<IEnumerable<BranchInfoItemDTO>>> GetBranchInfoItems()
    {
        return await _context.BranchInfoItems.Select(x => ItemToDTO(x)).ToListAsync();
    }

    // GET: api/BranchInfoItems/5
    [HttpGet("{id}")]
    public async Task<ActionResult<BranchInfoItemDTO>> GetBranchInfoItem(int id)
    {
        var branchInfoItem = await _context.BranchInfoItems.FindAsync(id);

        if (branchInfoItem == null)
        {
            return NotFound();
        }

        return ItemToDTO(branchInfoItem);
    }

    // PUT: api/BranchInfoItems/5
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPut("{id}")]
    public async Task<IActionResult> PutBranchInfoItem(int id, BranchInfoItemDTO branchInfoItemDTO)
    {
        if (id != branchInfoItemDTO.BranchId)
        {
            return BadRequest();
        }

        _context.Entry(branchInfoItemDTO).State = EntityState.Modified;

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException) when (!BranchInfoItemExists(id))
        {
            return NotFound();
        }

        return NoContent();
    }

    // POST: api/BranchInfoItems
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPost]
    public async Task<ActionResult<BranchInfoItemDTO>> PostBranchInfoItem(BranchInfoItemDTO branchInfoItemDTO)
    {
        var branchInfoItem = new BranchInfoItem
        {
            BranchId = branchInfoItemDTO.BranchId,
            Name = branchInfoItemDTO.Name,
            Address = branchInfoItemDTO.Address,
            City = branchInfoItemDTO.City,
            State = branchInfoItemDTO.State,
            ZipCode = branchInfoItemDTO.ZipCode,
        };
        _context.BranchInfoItems.Add(branchInfoItem);
        await _context.SaveChangesAsync();

        return CreatedAtAction(nameof(GetBranchInfoItem), new { id = branchInfoItemDTO.BranchId },
            ItemToDTO(branchInfoItem));
    }

    // DELETE: api/BranchInfoItems/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteBranchInfoItem(int id)
    {
        var branchInfoItem = await _context.BranchInfoItems.FindAsync(id);
        if (branchInfoItem == null)
        {
            return NotFound();
        }

        _context.BranchInfoItems.Remove(branchInfoItem);
        await _context.SaveChangesAsync();

        return NoContent();
    }

    private bool BranchInfoItemExists(int id) =>
        (_context.BranchInfoItems?.Any(e => e.BranchId == id)).GetValueOrDefault();

    private static BranchInfoItemDTO ItemToDTO(BranchInfoItem branchInfoItem) =>
        new()
        {
            BranchId = branchInfoItem.BranchId,
            Name = branchInfoItem.Name,
            Address = branchInfoItem.Address,
            City = branchInfoItem.City,
            State = branchInfoItem.State,
            ZipCode = branchInfoItem.ZipCode,
        };
}