using System.ComponentModel.DataAnnotations;

namespace SalesManagementAPI.Models;

public class BranchInfoItem
{
    [Key] public int BranchId { get; set; }
    public string? Name { get; set; }
    public string? Address { get; set; }
    public string? City { get; set; }
    public string? State { get; set; }
    public string? ZipCode { get; set; }
    public string? Secret { get; set; }
}

public class BranchInfoItemDTO
{
    [Key] public int BranchId { get; set; }
    public string? Name { get; set; }
    public string? Address { get; set; }
    public string? City { get; set; }
    public string? State { get; set; }
    public string? ZipCode { get; set; }
}