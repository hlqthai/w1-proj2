using Microsoft.EntityFrameworkCore;

namespace SalesManagementAPI.Models;

public class BranchInfoContext : DbContext
{
    public BranchInfoContext(DbContextOptions<BranchInfoContext> options) : base(options) { }
    public DbSet<BranchInfoItem> BranchInfoItems { get; set; } = null!;
}